import { configure } from '@storybook/angular';
import { setCompodocJson } from '@storybook/addon-docs/angular';
import docJson from '../documentation.json';

setCompodocJson(docJson);

configure(require.context('../src', true, /\.stories\.([tj]s|mdx)$/), module);
