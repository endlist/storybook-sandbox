import { Component, OnInit, OnDestroy, OnChanges, ElementRef, ViewChild, Input } from '@angular/core';
import { Spinner } from 'spin.js';

@Component({
  selector: 'dgl-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit, OnDestroy, OnChanges {
  @Input() public left = 'calc(100%-1.5rem)';
  @Input() public top = '1.5rem';
  @Input() public isSpinning = true;
  public spinner: any; // the instance of a spin.js spinner

  @ViewChild('spinner', { static: true }) spinnerEl: ElementRef;

  public ngOnInit(): void {
    this.spinner = this.createSpinner();
    if (this.isSpinning) { this.start(); }
  }

  public ngOnDestroy(): void {
    this.stop();
  }

  public ngOnChanges(): void {
    this.toggle(this.isSpinning);
  }

  public createSpinner = (): any => {
    const dgOpts = {
      color: '#555555',
      fadeColor: 'transparent',
      lines: 12,
      radius: 5,
      length: 3,
      width: 2,
      left: this.left,
      top: this.top,
      className: '',
      speed: 0.5
    };

    return new Spinner(dgOpts);
  }

  public toggle(start: boolean) {
    if (!this.spinner) { return; }
    if (start) {
      this.start();
    } else {
      this.stop();
    }
  }

  public start() {
    this.spinner.spin(this.spinnerEl.nativeElement);
  }

  public stop() {
    this.spinner.stop();
  }

}
