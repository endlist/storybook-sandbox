import { boolean, withKnobs } from '@storybook/addon-knobs';
import { SpinnerComponent } from './spinner.component';

export default { 
  title: 'Test|Spinner',
  decorators: [withKnobs],
  parameters: {
    knobs: { disableDebounce: true },
  },
  includeStories: []
};

export const basic = () => {
  const isSpinning = boolean('Is Spinning', true);

  return {
    moduleMetadata: {
      entryComponents: [SpinnerComponent],
      declarations: [SpinnerComponent]
    },
    template: `
      <dgl-spinner [left]="left" [isSpinning]="isSpinning"></dgl-spinner>
    `,
    props: {
      isSpinning,
      left: '2rem'
    }
  };
};
basic.story = { name: 'Basic' };
