import { Button } from '@storybook/angular/demo';
import { withKnobs, text } from '@storybook/addon-knobs';

export default {
  title: 'Test|My Button',
  decorators: [withKnobs],
  parameters: {
    knobs: { disableDebounce: true },
  }
};

export const withText = () => {
  const label = text('label', 'Hello Button');

  return {
    moduleMetadata: {
      entryComponents: [Button],
      declarations: [Button]
    },
    template: `<Button>${label}</Button>`,
    props: {
      label,
    }
  };
};

export const withEmoji = () => ({
  component: Button,
  props: {
    text: '😀 😎 👍 💯'
  },
});
